import { Map } from './modules/map.js'

const initialCoordinates = [50, 11]
const initialZoom = 4

const map = new Map(initialCoordinates, initialZoom)
