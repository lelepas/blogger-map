import { Utils } from "./utils.js";
export class Map {
  constructor(initialCoordinates, initialZoom) {
    this.utils = new Utils();
    this.map = L.map("map").setView(initialCoordinates, initialZoom);
    this.popup = L.popup();
    this.marker = L.icon({
      iconUrl: "assets/flag.png",
      // shadowUrl: 'assets/flag-shadow.png',
      iconSize: [35, 50], // size of the icon
      shadowSize: [30, 27], // size of the shadow
      iconAnchor: [5, 50], // point of the icon which will correspond to marker's location
      shadowAnchor: [4, 20], // the same for the shadow
      popupAnchor: [13, -45], // point from which the popup should open relative to the iconAnchor
    });

    this.initializeMap();
    this.createMarker();

    // For Debug
    this.initializeEvents();
  }

  initializeMap() {
    L.tileLayer(
      "https://server.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}",
      {
        attribution:
          "Tiles &copy; Esri &mdash; National Geographic, Esri, DeLorme, NAVTEQ, UNEP-WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, iPC",
        maxZoom: 6,
      }
    ).addTo(this.map);
  }

  initializeEvents() {
    this.map.on("click", this.onMapClick.bind(this));
  }

  loadData() {
    return this.utils.getData("data/data.json");
  }

  createMarker() {
    this.loadData()
      .then((countries) => {
        countries.forEach((country) => {
          this.utils
            .getData(
              `https://www.googleapis.com/blogger/v3/blogs/978249147746647750/posts?key=AIzaSyBR8xqjbGGA_PCaM2ercNoqQopkcnwtQsU&maxResults=500&labels=${country.name}`
            )
            .then((results) => {
              this.showMarker(country, results);
            })
            .catch((error) => {
              console.error(error);
            });
        });
      })
      .catch((error) => {
        console.error(error);
      });
  }

  showMarker(country, results) {
    const marker = L.marker([country.lat, country.lng], {
      icon: this.marker,
    })
      .addTo(this.map)
      .bindPopup(
        `${country.emoji}${country.name}${country.emoji}<br> ${results.items.length}件の記事があります <br> <a href="https://www.kageori.com/search/label/${country.name}?&max-results=6" target="_parent">記事を読む</a>`
      );
  }

  // For Debug
  onMapClick(e) {
    this.popup
      .setLatLng(e.latlng)
      .setContent("You clicked the map at " + e.latlng.toString())
      .openOn(this.map);
  }
}
