export class Utils {
  constructor() {}

  async getData(url) {
    try {
      return await new Promise((resolve, reject) => {
        let request = new XMLHttpRequest();
        request.open('GET', url, true);
        request.onload = () => {
          if (request.status >= 200 && request.status < 400) {
            let data = JSON.parse(request.responseText);
            resolve(data);
          } else {
            reject('Request error');
          }
        };
        request.onerror = () => {
          reject('Network error');
        };
        request.send();
      });
    } catch (error) {
      console.error(error);
      throw error;
    }
  }
}