# 追加手順

### ローカル開発環境の下準備

public/js/modules/map.js の getData()の取得リンクを dev 用のに置き換える（dev.md 参照）

https://console.cloud.google.com/apis/credentials にアクセスして、Dev-api の API キーを触れる状態にしておく。

### データの追加

public/js/modules/map.js の this.initializeEvents()のコメントアウトをはずして、クリックしたところの緯度経度を取得して、データに追加する。

取得し終わったら再びコメントアウトしておく。

以下のような形でデータを追加する。

,{
"lat": 47.368594,
"lng": 14.150391,
"name": "オーストリア",
"emoji":"🇦🇹"
}


